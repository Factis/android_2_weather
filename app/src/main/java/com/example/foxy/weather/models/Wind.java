package com.example.foxy.weather.models;

import com.google.gson.annotations.SerializedName;

public class Wind {

    @SerializedName("speed") private float speed;
    @SerializedName("deg") private float direction;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDirection() {
        return direction;
    }

    public void setDirection(float direction) {
        this.direction = direction;
    }
}

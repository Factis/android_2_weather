package com.example.foxy.weather.fragments;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.foxy.weather.App;
import com.example.foxy.weather.R;
import com.example.foxy.weather.data.CustomViewCircle;
import com.example.foxy.weather.utils.Constants;


public class SensorFragment extends Fragment {

    private SensorManager sensorManager;
    private Sensor sensorTemperature, sensorHumidity;
    private CustomViewCircle customViewTemperature, customViewHumidity;
    private final SensorEventListener sensorListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_AMBIENT_TEMPERATURE:
                    String temperature = String.valueOf(event.values[0]);
                    customViewTemperature.setColor(calculateTemperatureColor(valueToInt(temperature)));
                    customViewTemperature.setText(temperature);
                    customViewTemperature.invalidate();
                    return;
                case Sensor.TYPE_RELATIVE_HUMIDITY:
                    String humidity = String.valueOf(event.values[0]);
                    customViewHumidity.setColor(calculateHumidityColor(valueToInt(humidity)));
                    customViewHumidity.setText(humidity);
                    customViewHumidity.invalidate();
                    return;
                default:
                    break;
            }
        }

        @Override
        public void onAccuracyChanged(Sensor arg0, int arg1) {
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sensor, container, false);
        initUi(view);
        return view;
    }

    public void initUi(View view) {
        customViewTemperature = view.findViewById(R.id.fragment_sensor_custom_temperature);
        customViewHumidity = view.findViewById(R.id.fragment_sensor_custom_humidity);
        sensorManager = (SensorManager) App.instance().getSystemService(Context.SENSOR_SERVICE);
        sensorTemperature = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sensorHumidity = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(sensorListener, sensorTemperature, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(sensorListener, sensorHumidity, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(sensorListener);
    }

    private int calculateTemperatureColor(int redValue) {
        if (redValue <= Constants.COLD_TEMP) {
            return Color.rgb(Constants.COLD_RED, Constants.GREEN, Constants.BLUE);
        } else if (redValue <= Constants.MEDIUM_TEMP) {
            int step = Constants.MAX_RGB_COLOR / Constants.MEDIUM_TEMP;
            return Color.rgb(redValue * step, Constants.GREEN, Constants.BLUE);
        } else {
            return Color.rgb(Constants.MAX_RGB_COLOR, Constants.GREEN, Constants.BLUE);
        }
    }

    private int calculateHumidityColor(int value) {
        int greenValue = Constants.MAX_HUMIDITY - value;
        return Color.rgb(0, greenValue, Constants.MAX_RGB_COLOR);
    }

    private int valueToInt(String value) {
        return (int) Float.parseFloat(value);
    }
}

package com.example.foxy.weather.models;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Weather {

    @SerializedName("coord") public Coordinates coord;
    @SerializedName("weather") public List<WeatherDescription> descriptions;
    @SerializedName("main") public MainWeatherValues main;
    @SerializedName("wind") public Wind wind;
    @SerializedName("clouds") public Clouds clouds;
    @SerializedName("sys") public SystemValues sys;
    @SerializedName("visibility") private int visibility;
    @SerializedName("dt") private long lastUpdate;
    @SerializedName("id") private long cityId;
    @SerializedName("name") private String cityName;


    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long dt) {
        this.lastUpdate = dt;
    }

    public String getFormatLastUpdate() {
        return new SimpleDateFormat("HH:mm   dd.MM.yy", Locale.ROOT).format(new Date(lastUpdate * 1000));
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long id) {
        this.cityId = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String name) {
        this.cityName = name;
    }

}

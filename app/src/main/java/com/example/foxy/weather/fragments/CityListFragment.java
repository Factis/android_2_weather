package com.example.foxy.weather.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.foxy.weather.R;
import com.example.foxy.weather.list.CityListAdapter;
import com.example.foxy.weather.models.City;
import com.example.foxy.weather.utils.Constants;
import com.example.foxy.weather.utils.ToastUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CityListFragment extends Fragment {

    @BindView(R.id.fragment_city_list_recycler_view) RecyclerView recyclerView;
    private final List<City> cityList;
    private onFragmentInteractionListener listener;
    private Unbinder unbinder;

    public CityListFragment() {
        cityList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUi();
        return view;
    }

    private void initUi() {
        CityListAdapter adapter = new CityListAdapter(cityList, this::onListItemListener);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    private void onListItemListener(int cityId) {
        if (listener != null) {
            listener.onFragmentInteraction(cityId);
        }
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        if (args != null) {
            Serializable collection = args.getSerializable(Constants.CITY_LIST);
            if (collection instanceof List) {
                if (!((List) collection).isEmpty() && ((List) collection).get(0) instanceof City) {
                    List<City> list = (List<City>) collection;
                    cityList.clear();
                    cityList.addAll(list);
                }
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_city_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_fragment_city_list_search:
                ToastUtils.shortInfoToast(getString(R.string.search));
                return true;
            case R.id.menu_fragment_city_list_location:
                ToastUtils.shortInfoToast(getString(R.string.get_location));
                return true;
            default:
                return false;
        }
    }

    public void setListener(onFragmentInteractionListener listener) {
        this.listener = listener;
    }

    public interface onFragmentInteractionListener {
        void onFragmentInteraction(int cityId);

    }
}

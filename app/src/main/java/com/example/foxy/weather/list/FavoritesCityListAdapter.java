package com.example.foxy.weather.list;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.foxy.weather.R;
import com.example.foxy.weather.data.WeatherOkHttpClient;
import com.example.foxy.weather.models.City;
import com.example.foxy.weather.models.WeatherList;

import java.util.ArrayList;

public class FavoritesCityListAdapter extends RecyclerView.Adapter<FavoritesCityListAdapter.FavoritesCityListViewHolder> {

    private final ArrayList<City> favoritesCity;
    private ItemClickListener clickListener;
    private ItemLongClickListener longClickListener;

    public FavoritesCityListAdapter(ArrayList<City> favoritesCity, ItemClickListener clickListener, ItemLongClickListener longClickListener) {
        this.favoritesCity = favoritesCity;
        this.clickListener = clickListener;
        this.longClickListener = longClickListener;
    }

    @NonNull
    @Override
    public FavoritesCityListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_favorites_city, viewGroup, false);
        return new FavoritesCityListAdapter.FavoritesCityListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoritesCityListAdapter.FavoritesCityListViewHolder holder, int i) {
        holder.bindView(i);
    }

    @Override
    public int getItemCount() {
        return favoritesCity.size();
    }


    public interface ItemClickListener {
        void onItemClick(int position);
    }

    public interface ItemLongClickListener {
        void onItemLongClick(int position);
    }

    class FavoritesCityListViewHolder extends RecyclerView.ViewHolder {
        final ImageView weatherImage;
        final TextView cityName;
        final TextView temperature;

        FavoritesCityListViewHolder(@NonNull View itemView) {
            super(itemView);
            weatherImage = itemView.findViewById(R.id.item_favorites_weather_image);
            cityName = itemView.findViewById(R.id.item_favorites_city_name);
            temperature = itemView.findViewById(R.id.item_favorites_temperature);
        }

        void bindView(int position) {
            StringBuilder cityId = new StringBuilder();
            String prefix = "";
            for (int i = 0; i < favoritesCity.size(); i++) {
                cityId.append(prefix);
                prefix = ",";
                cityId.append(favoritesCity.get(i).getCityId());
            }

            final Handler handler = new Handler();
            new Thread(() -> {
                WeatherList weather = new WeatherOkHttpClient().getWeatherList(cityId.toString());
                handler.post(() -> temperature.setText(String.valueOf(weather.list.get(position).main.getTemp())));
            }).start();

            weatherImage.setImageResource(R.drawable.ic_weather_cloudy);
            cityName.setText(favoritesCity.get(position).getCityName());

            itemView.setOnClickListener(v -> clickListener.onItemClick(getAdapterPosition()));

            itemView.setOnLongClickListener(v -> {
                longClickListener.onItemLongClick(getAdapterPosition());
                return true;
            });

        }


    }


}


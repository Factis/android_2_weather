package com.example.foxy.weather.models;

import java.io.Serializable;


public class City implements Serializable {

   private String cityName;
   private long cityId;
   private String region;
    private String country;
    private float lon;
    private float lat;
    private String weather;
    private int image;

    public City() {
    }

    public City(String title, String detail, int image) {
        this.cityName = title;
        this.region = detail;
        this.image = image;
    }

    public City(String title) {
        this.cityName = title;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }
}

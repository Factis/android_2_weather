package com.example.foxy.weather.models;

import com.google.gson.annotations.SerializedName;

public class MainWeatherValues {

    @SerializedName("temp_min") float tempMin;
    @SerializedName("temp_max") float tempMax;
    @SerializedName("pressure") float pressure;
    @SerializedName("humidity") float humidity;
    @SerializedName("temp") private double temp;

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public float getTempMin() {
        return tempMin;
    }

    public void setTempMin(float tempMin) {
        this.tempMin = tempMin;
    }

    public float getTempMax() {
        return tempMax;
    }

    public void setTempMax(float tempMax) {
        this.tempMax = tempMax;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }
}

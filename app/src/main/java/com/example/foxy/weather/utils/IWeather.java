package com.example.foxy.weather.utils;

import com.example.foxy.weather.models.Weather;
import com.example.foxy.weather.models.WeatherList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IWeather {

    @GET("weather")
    Call<Weather> getWeatherByCity(@Query("q") String city, @Query("units") String units);

    @GET("group")
    Call<WeatherList> getWeatherByGroupCity(@Query("id") String citiesId, @Query("units") String units);

}

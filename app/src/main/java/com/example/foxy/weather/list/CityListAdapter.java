package com.example.foxy.weather.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.foxy.weather.R;
import com.example.foxy.weather.models.City;

import java.util.List;

public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.CityListViewHolder> {

    private final List<City> cityList;
    private ItemClickListener clickListener;

    public CityListAdapter(List<City> cityList, ItemClickListener clickListener) {
        this.cityList = cityList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CityListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_city_list, viewGroup, false);
        return new CityListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CityListViewHolder holder, int i) {
        holder.bindView(cityList.get(i));
    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }

    public interface ItemClickListener {
        void onItemClick(int position);
    }


    class CityListViewHolder extends RecyclerView.ViewHolder {
        final ImageView image;
        final TextView title;
        final TextView detail;

        CityListViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.item_city_list_image);
            title = itemView.findViewById(R.id.item_city_list_title);
            detail = itemView.findViewById(R.id.item_city_list_detail);
        }

        void bindView(City city) {
            image.setImageResource(city.getImage());
            title.setText(city.getCityName());
            detail.setText(city.getRegion());
            itemView.setOnClickListener(v -> clickListener.onItemClick(getAdapterPosition()));
        }

    }


}

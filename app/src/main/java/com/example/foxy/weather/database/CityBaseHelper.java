package com.example.foxy.weather.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.foxy.weather.database.CityDbSchema.CityTable;

public class CityBaseHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DB_NAME = "cityBase.db";

    CityBaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder createTable = new StringBuilder();
        createTable
                .append("CREATE TABLE ")
                .append(CityTable.NAME)
                .append(" (")
                .append(CityTable.Cols._ID)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
                .append(CityTable.Cols.CITY_NAME)
                .append(", ")
                .append(CityTable.Cols.CITY_ID)
                .append(", ")
                .append(CityTable.Cols.COUNTRY)
                .append(", ")
                .append(CityTable.Cols.WEATHER)
                .append(" );");
        db.execSQL(createTable.toString());

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

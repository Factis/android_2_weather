package com.example.foxy.weather.activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.foxy.weather.R;
import com.example.foxy.weather.data.Preferences;
import com.example.foxy.weather.fragments.CityListFragment;
import com.example.foxy.weather.fragments.DetailFragment;
import com.example.foxy.weather.fragments.FavoritesFragment;
import com.example.foxy.weather.fragments.SensorFragment;
import com.example.foxy.weather.fragments.SettingsFragment;
import com.example.foxy.weather.models.City;
import com.example.foxy.weather.utils.Constants;
import com.example.foxy.weather.utils.ToastUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private Toolbar toolBar;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;

    private final List<City> cityList;
    private CityListFragment cityListFragment;
    private DetailFragment detailFragment;
    private SensorFragment sensorFragment;
    private FavoritesFragment favoritesFragment;
    private SettingsFragment settingsFragment;
    private ArrayList<String> favCity;

    public MainActivity() {
        cityList = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addToList();
        initUI();
        onNavigationMenuClick();
    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toolBar = findViewById(R.id.toolbar);

        drawerToggle = setupDrawerToggle();
        drawerLayout.addDrawerListener(drawerToggle);

        setSupportActionBar(toolBar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        cityListFragment = new CityListFragment();
        detailFragment = new DetailFragment();
        sensorFragment = new SensorFragment();
        favoritesFragment = new FavoritesFragment();
        settingsFragment = new SettingsFragment();


//        loadPref(preferences);
        checkStart();
    }

    private void addCity(String title, String detail, int image) {
        cityList.add(new City(title, detail, image));
    }

    private void addToList() {
        int image = R.drawable.ic_default_city_img;
        String[] cities = getResources().getStringArray(R.array.cities);
        String[] detail = getResources().getStringArray(R.array.cities_detail);
        for (int i = 0; i < cities.length; i++) {
            addCity(cities[i], detail[i], image);
        }
    }

    private void setFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    private void replaceOnListFragment() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.CITY_LIST, (Serializable) cityList);
        cityListFragment.setArguments(bundle);
        cityListFragment.setListener(this::replaceOnDetailFragment);
        setFragment(cityListFragment);
    }

    private void replaceOnDetailFragment(int id) {
        Bundle bundle = new Bundle();
        if (favoritesFragment.isAdded()) {
            bundle.putString("test_json", favCity.get(id));
//            bundle.putString(Constants.FAVORITES_CITY, favoritesCity.get(id));
        } else {
            bundle.putSerializable(Constants.CITY_ID, cityList.get(id));
        }
        detailFragment.setArguments(bundle);
        setFragment(detailFragment);
    }

    private void replaceOnFavoritesFragment() {

        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constants.FAVORITES_CITY, favCity);
        favoritesFragment.setArguments(bundle);
        setFragment(favoritesFragment);
    }

    @Override
    public void onBackPressed() {
        if (detailFragment.isAdded() || sensorFragment.isAdded() || settingsFragment.isAdded()) {
            replaceOnListFragment();
            return;
        }
        super.onBackPressed();
    }

    private void onNavigationMenuClick() {
        navigationView.setNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.nav_choose_city:
                    replaceOnListFragment();
                    drawerLayout.closeDrawers();
                    return true;
                case R.id.nav_thermometer_and_humidity:
                    setFragment(sensorFragment);
                    drawerLayout.closeDrawers();
                    return true;
                case R.id.nav_favorites:
                    favCity = Preferences.instance().loadPref();
                    replaceOnFavoritesFragment();
                    drawerLayout.closeDrawers();
                    return true;
                case R.id.nav_settings:
                    setFragment(settingsFragment);
                    drawerLayout.closeDrawers();
                    return true;
                case R.id.nav_about:
                    ToastUtils.shortInfoToast(getString(R.string.about));
                    drawerLayout.closeDrawers();
                    return true;
                case R.id.nav_feedback:
                    ToastUtils.shortInfoToast(getString(R.string.send_feedback));
                    drawerLayout.closeDrawers();
                    return true;
                default:
                    return false;
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, drawerLayout, toolBar, R.string.open, R.string.close);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void checkStart() {
        favCity = Preferences.instance().loadPref();
//        if (!favoritesCity.isEmpty()) {
        if (favCity != null && !favCity.isEmpty()) {
//            replaceOnFavoritesFragment(favoritesCity);
            replaceOnFavoritesFragment();
        } else {
            replaceOnListFragment();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
package com.example.foxy.weather.utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import com.example.foxy.weather.App;
import com.example.foxy.weather.R;
import com.example.foxy.weather.models.Weather;

public class AppResources {

    private static int getWeatherImageID(Weather weather) {
        Resources resources = App.instance().getResources();
        String[] icons = resources.getStringArray(R.array.icon_weather);
        String icon;
        for (String icon1 : icons) {
            if (icon1.equals(weather.descriptions.get(0).getIcon())) {
                icon = Constants.WEATHER_ICON_DRAWABLE + icon1;
                return resources.getIdentifier(icon, "drawable", App.instance().getPackageName());
            }
        }
        return R.drawable.ic_weather_sunny;
    }

    private static int getWindImageID(float degrees) {
        Resources resources = App.instance().getResources();
        String[] icons = resources.getStringArray(R.array.wind_direction);
        String icon = Constants.WIND_ICON_DRAWABLE + icons[(int) Math.round(((double) degrees % 360) / 45)];
        return resources.getIdentifier(icon, "drawable", App.instance().getPackageName());
    }


    public static Drawable getWeatherDrawable(Weather weather) {
        return App.instance().getResources().getDrawable(getWeatherImageID(weather));
    }

    public static Drawable getWindDrawable(float degrees) {
        return App.instance().getResources().getDrawable(getWindImageID(degrees));
    }
}

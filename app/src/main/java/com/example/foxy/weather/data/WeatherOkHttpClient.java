package com.example.foxy.weather.data;

import com.example.foxy.weather.models.Weather;
import com.example.foxy.weather.models.WeatherList;
import com.example.foxy.weather.utils.Constants;
import com.example.foxy.weather.utils.IWeather;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

public class WeatherOkHttpClient {

    public Weather getWeather(String city) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(getInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.WEATHER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        IWeather iWeather = retrofit.create(IWeather.class);
        Call<Weather> call = iWeather.getWeatherByCity(city, Constants.WEATHER_UNNITS_METRIC);

        try {
            Response<Weather> response = call.execute();
            Weather answer = response.body();
            if (answer != null) {
                return answer;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public WeatherList getWeatherList(String citiesId) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(getInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.WEATHER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        IWeather iWeather = retrofit.create(IWeather.class);
        Call<WeatherList> call = iWeather.getWeatherByGroupCity(citiesId, Constants.WEATHER_UNNITS_METRIC);

        try {
            Response<WeatherList> response = call.execute();
            WeatherList answer = response.body();
            if (answer != null) {
                return answer;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Interceptor getInterceptor() {
        return chain -> {
            Request request = chain.request();
            HttpUrl url = request.url();
            String query = url.query();
            if (query != null && !query.contains("APPID")) {
                url = url.newBuilder()
                        .addQueryParameter("APPID", Constants.WEATHER_APPID)
                        .build();
                request = request.newBuilder().url(url).build();
            }
            return chain.proceed(request);
        };
    }

}

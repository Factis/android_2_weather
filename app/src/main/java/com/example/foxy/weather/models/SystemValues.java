package com.example.foxy.weather.models;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SystemValues {

    @SerializedName("country") private String country;
    @SerializedName("sunrise") private long sunrise;
    @SerializedName("sunset") private long sunset;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getSunrise() {
        return sunrise;
    }

    public void setSunrise(long sunrise) {
        this.sunrise = sunrise;
    }

    public long getSunset() {
        return sunset;
    }

    public void setSunset(long sunset) {
        this.sunset = sunset;
    }

    public String getFormatSunriseDate() {
        return new SimpleDateFormat("HH:mm").format(new Date(sunrise * 1000L));
    }

    public String getFormatSunsetDate() {
        return new SimpleDateFormat("HH:mm").format(new Date(sunset * 1000L));
    }
}

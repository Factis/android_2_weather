package com.example.foxy.weather.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.foxy.weather.R;
import com.example.foxy.weather.data.Preferences;
import com.example.foxy.weather.data.WeatherOkHttpClient;
import com.example.foxy.weather.database.CityRepository;
import com.example.foxy.weather.models.City;
import com.example.foxy.weather.models.Weather;
import com.example.foxy.weather.utils.AppResources;
import com.example.foxy.weather.utils.Constants;
import com.example.foxy.weather.utils.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DetailFragment extends Fragment {

    @BindView(R.id.fragment_detail_city_name) TextView cityTitle;
    @BindView(R.id.fragment_detail_last_update) TextView lastUpdate;
    @BindView(R.id.fragment_detail_weather_icon) ImageView iconWeather;
    @BindView(R.id.fragment_detail_temp) TextView temp;
    @BindView(R.id.fragment_detail_coordinates) TextView coordinates;
    @BindView(R.id.fragment_detail_weather_type) TextView weatherType;
    @BindView(R.id.fragment_detail_wind) TextView wind;
    @BindView(R.id.fragment_detail_ic_wind) ImageView iconWind;
    @BindView(R.id.fragment_detail_units_wind) TextView unitsWind;
    @BindView(R.id.fragment_detail_pressure) TextView pressure;
    @BindView(R.id.fragment_detail_visibility) TextView visibility;
    @BindView(R.id.fragment_detail_humidity) TextView humidity;
    @BindView(R.id.fragment_detail_units_humidity) TextView unitsHumidity;
    @BindView(R.id.fragment_detail_min_temp) TextView minTemp;
    @BindView(R.id.fragment_detail_units_min_temp) TextView unitsMinTemp;
    @BindView(R.id.fragment_detail_max_temp) TextView maxTemp;
    @BindView(R.id.fragment_detail_units_max_temp) TextView unitsMaxTemp;
    @BindView(R.id.fragment_detail_sunrise) TextView sunrise;
    @BindView(R.id.fragment_detail_sunset) TextView sunset;
    private City city;
    private Handler handler = new Handler();
    private Unbinder unbinder;
    private SearchView searchView;
    private CityRepository db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        db = new CityRepository();
        checkDB(city.getCityName());
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bundle bundle = getArguments();
        if (bundle != null) {
            city = (City) bundle.getSerializable(Constants.CITY_ID);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    private void checkDB(String oneCity) {

        if (db.inBase(oneCity)) {
            Weather weatherDB = db.loadFromDB(oneCity);
            setWeather(weatherDB);
            renderWeatherData(oneCity);
        } else {
            renderWeatherData(oneCity);
        }
    }

    private void renderWeatherData(String oneCity) {
        new Thread(() -> {
            Weather weather = new WeatherOkHttpClient().getWeather(oneCity);
            if (weather != null) {
                handler.post(() -> setWeather(weather));
                db.addToDB(weather, oneCity);
            } else {
                handler.post(() -> ToastUtils.shortInfoToast("Нет подключения к интернету"));
            }
        }).start();

        db.close();
    }

    private void setWeather (Weather weather) {
        cityTitle.setText(weather.getCityName());
        lastUpdate.setText(String.valueOf(weather.getFormatLastUpdate()));
        temp.setText(String.valueOf(weather.main.getTemp()));
        String text = "[" + weather.coord.getLat() + ", " + weather.coord.getLon() + "]";
        coordinates.setText(text);
        weatherType.setText(String.valueOf(weather.descriptions.get(0).getWeatherTypeDescription()));
        wind.setText(String.valueOf(weather.wind.getSpeed()));
        pressure.setText(String.valueOf(weather.main.getPressure()));
        visibility.setText(String.valueOf(weather.getVisibility()));
        humidity.setText(String.valueOf(weather.main.getHumidity()));
        minTemp.setText(String.valueOf(weather.main.getTempMin()));
        maxTemp.setText(String.valueOf(weather.main.getTempMax()));
        sunrise.setText(String.valueOf(weather.sys.getFormatSunriseDate()));
        sunset.setText(String.valueOf(weather.sys.getFormatSunsetDate()));
        iconWeather.setImageDrawable(AppResources.getWeatherDrawable(weather));
        iconWind.setImageDrawable(AppResources.getWindDrawable(weather.wind.getDirection()));
        city.setCityId(weather.getCityId());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_detail, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_fragment_detail_search);
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }

        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                        checkDB(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String typedText) {
                    return false;
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_fragment_detail_share:
                shareInfo();
                return true;
            case R.id.menu_fragment_detail_search:
                ToastUtils.shortInfoToast(getString(R.string.search));
                return true;
            case R.id.menu_fragment_detail_refresh:
                renderWeatherData(city.getCityName());
                ToastUtils.shortInfoToast(getString(R.string.refresh));
                return true;
            case R.id.menu_fragment_detail_favorites:
                Preferences.instance().checkFavoritesList(city);
                return true;
            default:
                return false;
        }
    }

    private void shareInfo() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.weather_unknown));
        startActivity(intent);
    }

}

package com.example.foxy.weather.models;

import com.google.gson.annotations.SerializedName;

public class WeatherDescription {

    @SerializedName("id") private int weatherId;
    @SerializedName("main") private String weatherType;
    @SerializedName("description") private String weatherTypeDescription;
    @SerializedName("icon") private String icon;

    public int getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(int weatherId) {
        this.weatherId = weatherId;
    }

    public String getWeatherType() {
        return weatherType;
    }

    public void setWeatherType(String weatherType) {
        this.weatherType = weatherType;
    }

    public String getWeatherTypeDescription() {
        return weatherTypeDescription;
    }

    public void setWeatherTypeDescription(String weatherTypeDescription) {
        this.weatherTypeDescription = weatherTypeDescription;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}

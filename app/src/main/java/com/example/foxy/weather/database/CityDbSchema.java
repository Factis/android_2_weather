package com.example.foxy.weather.database;

class CityDbSchema {

    static final class CityTable {
        static final String NAME = "cities";

        static final class Cols {
            static final String _ID = "_id";
            static final String CITY_NAME = "cityName";
            static final String CITY_ID = "cityId";
            static final String COUNTRY = "country";
            static final String WEATHER = "weather";
        }
    }
}

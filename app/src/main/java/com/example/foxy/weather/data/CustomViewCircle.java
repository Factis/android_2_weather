package com.example.foxy.weather.data;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import com.example.foxy.weather.R;

public class CustomViewCircle extends View {
    private Paint paintCircle;
    private Paint paintText;
    private Rect textBoundRect;
    private float centerX, centerY, radius, textWidth, textHeight;
    private String text;
    private int color;

    public CustomViewCircle(Context context) {
        super(context);
        initUi(context);
        initAttrs(null);
    }

    public CustomViewCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(attrs);
        initUi(context);
    }

    public CustomViewCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(attrs);
        initUi(context);
    }

    private void initUi(Context context) {
        text = getResources().getString(R.string.no_sensor);

        textBoundRect = new Rect();

        paintCircle = new Paint();
        paintCircle.setColor(color);

        paintText = new Paint();
        paintText.setColor(context.getResources().getColor(R.color.colorPrimaryText));
        paintText.setTextSize(100);
    }

    private void initAttrs(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.CustomViewCircle);
            color = array.getColor(R.styleable.CustomViewCircle_circle_color, getResources().getColor(R.color.colorAccent));
            array.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        calculateCircleSize();
        canvas.drawCircle(centerX, centerY, radius, paintCircle);
        calculateTextOrientation();
        canvas.drawText(text, centerX - (textWidth / 2f), centerY + (textHeight / 2f), paintText);
    }

    private void calculateCircleSize() {
        float width, height;
        width = getWidth();
        height = getHeight();
        centerX = width / 2;
        centerY = height / 2;

        if (width > height) {
            radius = centerY;
        } else {
            radius = centerX;
        }
    }

    private void calculateTextOrientation() {
        paintText.getTextBounds(text, 0, text.length(), textBoundRect);
        textWidth = paintText.measureText(text);
        textHeight = textBoundRect.height();
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        paintCircle.setColor(color);
    }

    public void setText(String text) {
        this.text = text;
    }

}

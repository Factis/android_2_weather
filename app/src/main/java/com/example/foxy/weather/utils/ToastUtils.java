package com.example.foxy.weather.utils;

import android.view.Gravity;
import android.widget.Toast;

import com.example.foxy.weather.App;

public class ToastUtils {

    public static void shortInfoToast(String message) {
        Toast toast = Toast.makeText(App.instance(), message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}

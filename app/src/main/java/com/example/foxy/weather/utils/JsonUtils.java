package com.example.foxy.weather.utils;

import com.example.foxy.weather.models.Weather;
import com.google.gson.Gson;

public class JsonUtils {

    public static String objToJson(Weather weather) {
        Gson gson = new Gson();
        return gson.toJson(weather);
    }

    public static Weather jsonToObj(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Weather.class);
    }
}

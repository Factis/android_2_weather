package com.example.foxy.weather.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cocosw.bottomsheet.BottomSheet;
import com.example.foxy.weather.R;
import com.example.foxy.weather.data.Preferences;
import com.example.foxy.weather.list.FavoritesCityListAdapter;
import com.example.foxy.weather.models.City;
import com.example.foxy.weather.utils.Constants;
import com.example.foxy.weather.utils.ToastUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FavoritesFragment extends Fragment {

    @BindView(R.id.fragment_favorites_recycler_view) RecyclerView recyclerView;
    private FavoritesCityListAdapter adapter;
    private onFragmentInteractionListener listener;
    private ArrayList<City> favoritesCity;
    private Unbinder unbinder;

    public FavoritesFragment() {
        favoritesCity = new ArrayList<>();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUi();
        return view;
    }

    private void initUi() {
        adapter = new FavoritesCityListAdapter(favoritesCity, this::onItemListener, this::onItemLongClick);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        if (args != null) {
            List<String> cityName = args.getStringArrayList(Constants.FAVORITES_CITY);
            if (cityName != null && !cityName.isEmpty()) {
                favoritesCity.clear();
                Gson gson = new Gson();
                for (String string : cityName) {
                    City city = gson.fromJson(string, City.class);
                    favoritesCity.add(city);
                }

            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
        saveFavoritesList();
    }

    private void saveFavoritesList() {
        Preferences.instance().savePref(favoritesCity);
    }

    private void onItemListener(int cityId) {
        if (listener != null) {
            listener.onFragmentInteraction(cityId);
        }
    }

    private void onItemLongClick(int position) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            new BottomSheet.Builder(activity)
                    .sheet(R.menu.bottom_menu)
                    .listener((dialog, which) -> {
                        switch (which) {
                            case R.id.bottom_highlight:
                                ToastUtils.shortInfoToast(getString(R.string.highlight));
                                break;
                            case R.id.bottom_delete:
                                favoritesCity.remove(position);
                                saveFavoritesList();
                                adapter.notifyDataSetChanged();
                                break;
                        }
                    })
                    .show();
        }

        ToastUtils.shortInfoToast("long click");
    }

    public interface onFragmentInteractionListener {
        void onFragmentInteraction(int cityId);
    }
}

package com.example.foxy.weather.utils;

public interface Constants {
    String CITY_LIST = "city list";
    String CITY_ID = "city name";
    String LOG_TAG = "log_tag";
    String WEATHER_URL = "https://api.openweathermap.org/data/2.5/";
    String WEATHER_UNNITS_METRIC = "metric";
    String WEATHER_UNNITS_IMPERIAL = "imperial";
    String WEATHER_APPID = "9f918ba14dc2c41234ff4f5ca84d4678";
    String WEATHER_ICON_DRAWABLE = "ic_weather_";
    String WIND_ICON_DRAWABLE = "ic_wind_";
    int GREEN = 70;
    int BLUE = 100;
    int COLD_RED = 70;
    int MAX_RGB_COLOR = 255;
    int COLD_TEMP = 10;
    int MEDIUM_TEMP = 30;
    int MAX_HUMIDITY = 100;


//    preferences

    String PREF_FILE_NAME = "settings_app_weather";
    String FAVORITES_CITY = "favorites_city";
}

package com.example.foxy.weather.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.foxy.weather.App;
import com.example.foxy.weather.R;
import com.example.foxy.weather.models.City;
import com.example.foxy.weather.utils.Constants;
import com.example.foxy.weather.utils.ToastUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Preferences {

    private static volatile Preferences instance;
    private SharedPreferences preferences;

    private Preferences() {
        preferences = App.instance().getSharedPreferences(Constants.PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public static Preferences instance() {
        Preferences localInstance = instance;
        if (localInstance == null) {
            synchronized (Preferences.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Preferences();
                }
            }
        }
        return localInstance;
    }

    private void savePref(Set<String> citySet) {
        preferences
                .edit()
                .clear()
                .putStringSet(Constants.FAVORITES_CITY, citySet)
                .apply();
    }

    public void checkFavoritesList(City city) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(city);
        Set<String> citySet = preferences.getStringSet(Constants.FAVORITES_CITY, null);
        if (citySet != null && !citySet.isEmpty()) {
            for (int i = 0; i < citySet.size(); i++) {
                if (citySet.contains(jsonString)) {
                    ToastUtils.shortInfoToast(App.instance().getResources().getString(R.string.already_added_to_favorites));
                } else {
                    citySet.add(jsonString);
                    ToastUtils.shortInfoToast(App.instance().getResources().getString(R.string.city_add_to_favorites));
                }
            }
        } else {
            citySet = new HashSet<>();
            citySet.add(jsonString);
            ToastUtils.shortInfoToast(App.instance().getResources().getString(R.string.city_add_to_favorites));
        }
        savePref(citySet);
    }

    private Set<String> objToJson(List<City> list) {
        Gson gson = new Gson();
        Set<String> set = new HashSet<>();
        String string;
        for (int i = 0; i < list.size(); i++) {
            string = gson.toJson(list.get(i));
            set.add(string);
        }
        return set;
    }

    public ArrayList<String> loadPref() {
        ArrayList<String> favCity = new ArrayList<>();
        Set<String> citySet = preferences.getStringSet(Constants.FAVORITES_CITY, null);
        if (citySet != null && !citySet.isEmpty()) {
            favCity.clear();
            favCity.addAll(citySet);
        }
        return favCity;
    }

    public void savePref(ArrayList<City> favoritesCity) {
        Set<String> citySet = objToJson(favoritesCity);
        savePref(citySet);
    }
}

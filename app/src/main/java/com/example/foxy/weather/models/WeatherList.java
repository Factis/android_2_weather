package com.example.foxy.weather.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherList {

    @SerializedName("list") public List<Weather> list;
}

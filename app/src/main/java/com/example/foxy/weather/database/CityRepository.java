package com.example.foxy.weather.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.foxy.weather.App;
import com.example.foxy.weather.database.CityDbSchema.CityTable;
import com.example.foxy.weather.models.Weather;
import com.example.foxy.weather.utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

public class CityRepository {
    private CityBaseHelper cityBaseHelper;
    private SQLiteDatabase database;
    private boolean flag;

    public CityRepository() {
        cityBaseHelper = new CityBaseHelper(App.instance());
    }

    private List<String> getAllCity() {
        List<String> cities = new ArrayList<>();
        String[] arr = {CityTable.Cols.CITY_NAME};
        database = cityBaseHelper.getReadableDatabase();
        Cursor cursor = database.query(CityTable.NAME, arr, null, null, null, null, null, null);
        String city;
        boolean hasNext = cursor.moveToFirst();

        while (hasNext) {
            city = cursor.getString(cursor.getColumnIndex(CityTable.Cols.CITY_NAME));
            cities.add(city);
            hasNext = cursor.moveToNext();
        }

        cursor.close();
        return cities;
    }

    public boolean inBase (String query) {
        flag = false;
        List<String> cities = getAllCity();

        for (String city : cities) {
            if (city.equals(query)) {
                flag = true;
                break;
            } else flag = false;
        }
        return flag;
    }

    public void addToDB(Weather weather, String city) {
        if (!flag) {
            database = cityBaseHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(CityTable.Cols.CITY_NAME, city);
            values.put(CityTable.Cols.CITY_ID, weather.getCityId());
            values.put(CityTable.Cols.COUNTRY, weather.sys.getCountry());
            values.put(CityTable.Cols.WEATHER, JsonUtils.objToJson(weather));
            database.insert(CityTable.NAME, null, values);
        } else {
            updateWeatherInDB(weather, city);
        }
    }

    public Weather loadFromDB (String city) {
        database = cityBaseHelper.getReadableDatabase();
        Weather weather = null;

        String[] columns = {CityTable.Cols.WEATHER};
        Cursor cursor = database.query(CityTable.NAME, columns, CityTable.Cols.CITY_NAME + " =?", new String[]{city}, null, null, null );

        if (cursor.moveToFirst()) {
            String json = cursor.getString(cursor.getColumnIndex(CityTable.Cols.WEATHER));
            weather = JsonUtils.jsonToObj(json);
        }
        cursor.close();
            return weather;
    }

    private void updateWeatherInDB(Weather weather, String city) {
        database = cityBaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        Cursor cursor = database.query(CityTable.NAME, new String[]{CityTable.Cols.WEATHER}, CityTable.Cols.CITY_NAME + " =?", new String[]{city}, null, null, null );
        if (cursor.moveToFirst()) {
            String weatherDB = cursor.getString(cursor.getColumnIndex(CityTable.Cols.WEATHER));
            values.put(CityTable.Cols.WEATHER, JsonUtils.objToJson(weather));
            database.update(CityTable.NAME, values, CityTable.Cols.WEATHER + "=?", new String[]{weatherDB});
        }
        cursor.close();
    }


    public void close() {
        cityBaseHelper.close();
    }

}
